* Multi-sorted First order logic

In this set of assignments we capture multi-sorted First order
logic. For example consider the case of School Geometry where there
are two sort of objects Points and Lines.


NOTE:

You can use the notation ∀ (x : u) ψ(x), and ∃ (x : u) ψ(x) to say
"for all x of sort u ψ(x)", and "there exists x of sort u ψ (x)"



1. Formalise the notion of a multi-sorted First order signature.

   Hint: In the FO-signature that we saw in class there was only one
   sort and we could ignore it.  Now a signature Σ consists of a
   4-tuple Σ = (U, R , F , C). Think of the set U as a set of types in
   your language What you need to do is to associate "types" to
   constants, functions, and relation symbols. Example for the School
   geometry is 2-sorted (i.e. the cardinality of U is 2) where the
   universe of sort U = { Points, Lines}

2. For talking about terms in this setting we will have to think of the
   variable set V as the disjoint union of a family { Vₛ | s ∈ U }. Extend
   the notion of terms to this setting. Note that terms in you language should
   have an associated sort. Use the notation x:u to say that x is a variable in Vᵤ

3. How will you define semantics for multi-sorted logic ?

4. Formalise School Geometry as a 2-sorted logic where U = { Point, Line} with
   the following relational symbols

   -  Intersect(ℓ₁, ℓ₂, p) :: 3-ary relation which says that lines ℓ₁
        and ℓ₂ interset at point p

   -  IsOn(p, ℓ) :: 2-ary relation that says p is on the line ℓ.

5. Write a formula with two free variables ϕ(ℓ₁, ℓ₂) which captures
   the notion of the lines ℓ₁ and ℓ₂ being parallel. Using the above
   formula write a sentence (i.e. a formula with no free variables)
   which assets Euclid's parallel axis axiom. i.e Given a line ℓ and a
   point p not on ℓ there is one and only one line through p that is
   parallel to ℓ.

6. Encode Many sorted FO into 1-sorted FO-theory (i.e. the normal FO
   that we did in the class) + a set of axioms that are sentences in
   the FO-theory showing that these are equally powerful. Hint for
   each sort s ∈ U add a predicate Tₛ(x) which asserts that x is of
   sort s. You will in addition need the disjointedness axioms, which
   says that Tᵤ(x) and Tᵥ(x) cannot simultaneously be true for
   distinct u,v in U. So that way many-sortedness is just a
   convenience.
